const users = [
  {
    id: 1,
    name: "Hisan Ibrahim",
  },
  {
    id: 2,
    name: "Asif Mujthaba",
  },
];

const respondUsers = (req, res) => {
  res.status(200).send(users);
};

module.exports = { respondUsers };
