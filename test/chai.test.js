require('../bin/www');

const chai = require('chai');
const chaiHttp = require('chai-http');

const expect = chai.expect;
chai.use(chaiHttp);

// describe("App", function () {
//   it("has the default page", (done) => {
//     chai
//       .request(`http://localhost:3000`)
//       .get("/")
//       .end((err, res) => {
//         if (err) {
//           return done(err);
//         }
//         expect(res).to.have.status(200);
//         done();
//       });
//   });
// });

describe('Users', function () {
    it('get users list', (done) => {
        chai.request(`http://localhost:61000`)
            .get('/users/')
            .end((err, res) => {
                if (err) {
                    return done(err);
                }
                expect(res).to.have.status(200);
                done();
            });
    });
});
