const express = require("express");
const expressJSDocSwagger = require("express-jsdoc-swagger");

const options = {
  info: {
    version: "1.0.0",
    title: "Albums store",
    license: {
      name: "MIT",
    },
  },
  security: {
    BasicAuth: {
      type: "http",
      scheme: "basic",
    },
  },
  filesPattern: "./**/*.js", // Glob pattern to find your jsdoc files
  swaggerUIPath: "/api-docs", // SwaggerUI will be render in this url. Default: '/api-docs'
  baseDir: __dirname,
};

const app = express();
const PORT = 3000;

app.use(express.json());

expressJSDocSwagger(app)(options);

/**
 * A song type
 * @typedef {object} Song
 * @property {string} title.required - The title
 * @property {string} artist - The artist
 * @property {number} year - The year - double
 */

/**
 * GET /api/v1/albums
 * @summary This is the summary or description of the endpoint
 * @tags album
 * @return {array<Song>} 200 - success response - application/json
 */
app.get("/api/v1/albums", (req, res) =>
  res.json([
    {
      title: "abum 1",
    },
    {
      title: "abum 2",
    },
  ])
);

/**
 * GET /api/v1/albums/:id
 * @summary This is the summary or description of the endpoint
 * @security BasicAuth
 * @tags album
 * @param {number} id.path - album id
 * @return {object} 200 - success response - application/json
 * @return {object} 400 - Bad request response
 */
app.get("/api/v1/albums/:id", (req, res) =>
  res.json({
    title: "abum 1",
  })
);

/**
 * POST /api/v1/albums
 * @tags album
 * @param {array<Song>} request.body.required - songs info
 * @return {object} 200 - album response
 */
app.post("/api/v1/albums", (req, res) => res.send(req.body));

app.listen(PORT, () =>
  console.log(`Example app listening at http://localhost:${PORT}`)
);
