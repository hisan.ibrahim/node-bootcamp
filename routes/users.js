const express = require("express");
const router = express.Router();
const usersController = require("./users.controller");

/* GET users listing. */
router.get("/", usersController.respondUsers);

module.exports = router;
