const playwright = require('playwright');
const chai = require('chai');

const expect = chai.expect;
const clientUri = 'http://localhost:61000/';
// playwright variables
let page, browser, context;

describe('Student', async () => {
    it('should able to logout', async () => {
        browser = await playwright.chromium.launch({
            headless: true,
        });
        context = await browser.newContext();
        page = await context.newPage();

        await page.goto(clientUri);

        const title = 'div.jumbotron:nth-child(1) > h1:nth-child(1)';

        expect(title).is.exist;
    });
});
